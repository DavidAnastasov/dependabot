require "pathname"
require "dependabot/clients/github_with_retries"
require "dependabot/metadata_finders"
require "dependabot/pull_request_creator"
require "gitlab"

module Dependabot

	require "dependabot/metadata_finders"
	# Customize PullRequestCreator class
	class PullRequestCreator
		require "dependabot/pull_request_creator/azure"
		require "dependabot/pull_request_creator/codecommit"
		require "dependabot/pull_request_creator/github"
		require "dependabot/pull_request_creator/gitlab"
		require "dependabot/pull_request_creator/message_builder"
		require "dependabot/pull_request_creator/branch_namer"
		require "dependabot/pull_request_creator/labeler"

		class RepoNotFound < StandardError; end
		class RepoArchived < StandardError; end
		class RepoDisabled < StandardError; end
		class NoHistoryInCommon < StandardError; end

		attr_reader :source, :dependencies, :files, :base_commit,
			:credentials, :pr_message_header, :pr_message_footer,
			:custom_labels, :author_details, :signature_key,
			:commit_message_options, :vulnerabilities_fixed,
			:reviewers, :assignees, :milestone, :branch_name_separator,
			:branch_name_prefix, :github_redirection_service,
			:custom_headers, :custom_issue_id

		def initialize(source:, base_commit:, dependencies:, files:, credentials:,
			pr_message_header: nil, pr_message_footer: nil,
			custom_labels: nil, author_details: nil, signature_key: nil,
			commit_message_options: {}, vulnerabilities_fixed: {},
			reviewers: nil, assignees: nil, milestone: nil,
			branch_name_separator: "-", branch_name_prefix: "dependabot",
			label_language: false, automerge_candidate: false,
			github_redirection_service: "github-redirect.dependabot.com",
			custom_headers: nil, require_up_to_date_base: false, 
			custom_issue_id: nil)
			@dependencies               = dependencies
			@source                     = source
			@base_commit                = base_commit
			@files                      = files
			@credentials                = credentials
			@pr_message_header          = pr_message_header
			@pr_message_footer          = pr_message_footer
			@author_details             = author_details
			@signature_key              = signature_key
			@commit_message_options     = commit_message_options
			@custom_labels              = custom_labels
			@reviewers                  = reviewers
			@assignees                  = assignees
			@milestone                  = milestone
			@vulnerabilities_fixed      = vulnerabilities_fixed
			@branch_name_separator      = branch_name_separator
			@label_language             = label_language
			@automerge_candidate        = automerge_candidate
			@github_redirection_service = github_redirection_service
			@custom_headers             = custom_headers
			@require_up_to_date_base    = require_up_to_date_base
			new_issue                   = create_new_issue(dependencies)
			@custom_issue_id            = new_issue["iid"].to_s
			@branch_name_prefix         = new_issue["iid"].to_s

			check_dependencies_have_previous_version
		end

		# API request for a new issue, MUST pass dependecies array
		def create_new_issue(d)
			gitlab_hostname = ENV["GITLAB_HOSTNAME"] || "git.enectiva.cz"
			gitlab_token = ENV["GITLAB_TOKEN"] || ""
			project_id = ENV["PROJECT_ID"] || ""
			# generating a new issue related to the dep update
			new_issue = HTTParty.post(
				"https://#{gitlab_hostname}/api/v4/projects/#{project_id}/issues/",
				:query => {
					"title" => "Update #{d.first.name} to #{d.first.version}",
					"labels" => "Hamster wheel",
					"description" => "![](https://kutlime.files.wordpress.com/2016/02/amazing-hamster-wheel.gif)"
				},
				:headers => {
					"PRIVATE-TOKEN" => gitlab_token,
				}
			)
			new_issue
		end

		def message_builder
			@message_builder ||
				(builder = MessageBuilder.new(
					source: source,
					dependencies: dependencies,
					files: files,
					credentials: credentials,
					commit_message_options: commit_message_options,
					pr_message_header: pr_message_header,
					pr_message_footer: pr_message_footer,
					vulnerabilities_fixed: vulnerabilities_fixed,
					github_redirection_service: github_redirection_service
				))
			builder.custom_issue_id = @custom_issue_id
			builder
		end

		# Customize MR/Commit message
		class MessageBuilder

			require "dependabot/pull_request_creator/message_builder/issue_linker"
			require "dependabot/pull_request_creator/message_builder/link_and_mention_sanitizer"
			require "dependabot/pull_request_creator/pr_name_prefixer"

			attr_writer :custom_issue_id

			attr_reader :source, :dependencies, :files, :credentials,
				:pr_message_header, :pr_message_footer,
				:commit_message_options, :vulnerabilities_fixed,
				:github_redirection_service, :gitlab_hostname, :repo_path

			def initialize(source:, dependencies:, files:, credentials:,
				pr_message_header: nil, pr_message_footer: nil,
				commit_message_options: {}, vulnerabilities_fixed: {},
				github_redirection_service: nil)
				@dependencies               = dependencies
				@files                      = files
				@source                     = source
				@credentials                = credentials
				@pr_message_header          = pr_message_header
				@pr_message_footer          = pr_message_footer
				@commit_message_options     = commit_message_options
				@vulnerabilities_fixed      = vulnerabilities_fixed
				@github_redirection_service = github_redirection_service
				@gitlab_hostname			= ENV["GITLAB_HOSTNAME"] || "git.enectiva.cz"
				@repo_path					= ENV["REPO_PATH"] || ""
			end

			# Issue link line
			def get_first_line
				return "https://#{@gitlab_hostname}/#{@repo_path}/issues/#{@custom_issue_id} \n\n"
			end

			def requirement_commit_message_intro
				msg = get_first_line
				msg += "\n\nUpdates the requirements on "

				msg +=
					if dependencies.count == 1
						"#{dependency_links.first} "
					else
						"#{dependency_links[0..-2].join(', ')} and #{dependency_links[-1]} "
					end

				msg + "to permit the latest version."
			end

			def version_commit_message_intro
				msg = get_first_line
				if dependencies.count > 1 && updating_a_property?
					return msg+multidependency_property_intro
				end

				if dependencies.count > 1 && updating_a_dependency_set?
					return msg+dependency_set_intro
				end

				return msg+multidependency_intro if dependencies.count > 1

				dependency = dependencies.first
				msg += "Update #{dependency_links.first} "\
              		"#{previous_version(dependency)} "\
              		"-> #{new_version(dependency)}."

				if switching_from_ref_to_release?(dependency)
					msg += " This release includes the previously tagged commit."
				end

				if vulnerabilities_fixed[dependency.name]&.one?
					msg += " **This update includes a security fix.**"
				elsif vulnerabilities_fixed[dependency.name]&.any?
					msg += " **This update includes security fixes.**"
				end

				msg
			end

			def application_pr_name
				pr_name = pr_name_prefix
				pr_name = pr_name.capitalize if pr_name_prefixer.capitalize_first_word?

				pr_name +
					if dependencies.count == 1
						dependency = dependencies.first
						"#{dependency.display_name} #{previous_version(dependency)} "\
            				"-> #{new_version(dependency)}"
					elsif updating_a_property?
						dependency = dependencies.first
						"#{property_name} #{previous_version(dependency)} "\
            				"-> #{new_version(dependency)}"
					elsif updating_a_dependency_set?
						dependency = dependencies.first
						"#{dependency_set.fetch(:group)} dependency set "\
							"#{previous_version(dependency)} "\
							"-> #{new_version(dependency)}"
					else
						names = dependencies.map(&:name)
						"#{names[0..-2].join(', ')} and #{names[-1]}"
					end
			end

			def library_pr_name
				pr_name = pr_name_prefix
				pr_name = pr_name.capitalize if pr_name_prefixer.capitalize_first_word?

				pr_name +
					if dependencies.count == 1
						"#{dependencies.first.display_name} "\
            				"#{old_library_requirement(dependencies.first)} "\
            				"-> #{new_library_requirement(dependencies.first)}"
					else
						names = dependencies.map(&:name)
						"requirements for #{names[0..-2].join(', ')} and #{names[-1]}"
					end
			end

			def metadata_links_for_dep(dep)
				msg = ""
				msg += "\n- Release notes: #{releases_url(dep)}" if releases_url(dep)
				msg += "\n- Changelog: #{changelog_url(dep)}" if changelog_url(dep)
				msg += "\n- Upgrade guide:#{upgrade_url(dep)}" if upgrade_url(dep)
				msg += "\n- Commits: #{commits_url(dep)}" if commits_url(dep)
				msg
			end

			def dependency_links
				dependencies.map do |dependency|
					if source_url(dependency)
						"#{dependency.display_name} (#{source_url(dependency)})"
					elsif homepage_url(dependency)
						"#{dependency.display_name} (#{homepage_url(dependency)})"
					else
						dependency.display_name
					end
				end
			end

		end

		# Customize branch name
		class BranchNamer

			attr_reader :dependencies, :files, :target_branch, :separator, :prefix

			def initialize(dependencies:, files:, target_branch:, separator: "-",
						   prefix: "")
				@dependencies  = dependencies
				@files         = files
				@target_branch = target_branch
				@separator     = separator
				@prefix        = prefix
			end

			def prefixes
				[
					prefix,
					target_branch
				].compact
			end

		end

		class Gitlab

			def create_merge_request
				gitlab_client_for_source.create_merge_request(
					source.repo,
					pr_name,
					source_branch: branch_name,
					target_branch: ENV["TARGET_BRANCH"] || default_branch,
					description: pr_description,
					remove_source_branch: true,
					assignee_id: assignee,
					labels: labeler.labels_for_pr.join(","),
					milestone_id: milestone
				)
			end

		end

	end

end