# Dependabot scripts
It handles the logic for updating dependencies in Gitlab.
## Installation 
1. Create a new schedule in CI/CD -> Schedule -> [New schedule](https://git.enectiva.cz/open/dependabot/pipeline_schedules/new) and set up these variables:
    * `PACKAGE_MANAGER_SET`: names of the package managers you want to check, separated by a coma with no blank spaces (the full list of available options is in the [.gitlab-ci.yml](https://git.enectiva.cz/open/dependabot/blob/master/.gitlab-ci.yml))
    * `DEP_PATH`: package manager dependencies file's path
    * `PROJECT_ID`: Id of the target project
    * `REPO_PATH`: path of the repository in this format -> namespace/repository (ex. enectiva/enectiva)
    * `BRANCH_NAME`: the name of the source branch in the remote repository, if the source is the `master` branch just don't set it (remove the variable).
    * `TARGET_BRANCH`: the name of the target branch in the remote repository, if the target is equal to the source branch just don't set it (remove the variable).
    * `BLACKLIST`: comma separated dependencies' names that won't be updated (ex. resque,ancestry). If there are no blacklisted libraries you can remove this variable.
2. Enjoy!
## Blacklist
If you think that blacklist some libraries could be a confusing method, you can also leave the issue, of the "unwanted" library, open. In this way Dependabot won't open another merge request or create a new branch for the update of that library. You can add some comments to the issue to explain why you don't want to update that library.

Feel free to close the related merge request and delete the branch created by Dependabot, the only important thing is the issue that have to remain OPENED.