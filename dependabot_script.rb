require "dependabot/file_fetchers"
require "dependabot/file_parsers"
require "dependabot/update_checkers"
require "dependabot/file_updaters"
require "dependabot/pull_request_creator"
require "dependabot/omnibus"
require "gitlab"
require "./monkey_patch.rb"

github_token = ENV["GITHUB_TOKEN"] || ""
gitlab_token = ENV["GITLAB_TOKEN"] || ""
gitlab_hostname = ENV["GITLAB_HOSTNAME"] || "git.enectiva.cz"
project_id = ENV["PROJECT_ID"] || ""
package_manager = ENV["PACKAGE_MANAGER"] || ""
repo_name = ENV["REPO_PATH"] || "" # namespace/project - Full name of the repo you want to create pull requests for.
directory = ENV["DEP_PATH"] || "" # Directory where the base dependency files are.
branch = ENV["SOURCE_BRANCH"] || nil
blacklist = ENV["BLACKLIST"] || nil

def issueAlreadyExists?(glHostname, glToken, projectID, depName)
	######################################
	# Check if this ISSUE ALREADY exists #
	######################################
	search_string = "Update #{depName}"
	issues = HTTParty.get(
		"https://#{glHostname}/api/v4/projects/#{projectID}/issues/",
		:query => {
			"state" => "opened",
			"search" => search_string,
			"in" => "title",
		},
		:headers => {
			"PRIVATE-TOKEN" => glToken,
		}
	)

	if issues.any?
		issues.each do |i|
			old_title = i["title"].split
			old_2w = old_title[0] + " " + old_title[1]
			if search_string == old_2w
				return true
			end
		end
	end
	return false
end

def isDepBlacklisted?(bl, dep_name)
	if bl != nil
    	list = bl.split(",")
    	list.each do |elem|
    		if elem == dep_name
    			return true
    		end
    	end
    end
    return false
end

# Name of the package manager you'd like to do the update for. Options are:
# - bundler
# - pip (includes pipenv)
# - npm_and_yarn
# - maven
# - gradle
# - cargo
# - hex
# - composer
# - nuget
# - dep
# - go_modules
# - elm
# - submodules
# - docker
# - terraform

credentials = [
  	{
		"type" => "git_source",
		"host" => "github.com",
		"username" => "x-access-token",
		"password" => github_token # A GitHub access token with read access to public repos
  	}
]

if gitlab_token
	credentials << {
		"type" => "git_source",
		"host" => gitlab_hostname,
		"username" => "x-access-token",
		"password" => gitlab_token # A GitLab access token with API permission
	}

  	source = Dependabot::Source.new(
		provider: "gitlab",
		hostname: gitlab_hostname,
		api_endpoint: "https://#{gitlab_hostname}/api/v4",
		repo: repo_name,
		directory: directory,
		branch: branch,
  	)
else
	puts "Please provide a valid GitLab Token"
end

##############################
# Fetch the dependency files #
##############################
puts "Fetching #{package_manager} dependency files for #{repo_name}"
fetcher = Dependabot::FileFetchers.for_package_manager(package_manager).new(
  	source: source,
  	credentials: credentials,
)

files = fetcher.files
commit = fetcher.commit

##############################
# Parse the dependency files #
##############################
puts "Parsing dependencies information"
parser = Dependabot::FileParsers.for_package_manager(package_manager).new(
  	dependency_files: files,
  	source: source,
  	credentials: credentials,
)


begin
    dependencies = parser.parse
rescue Dependabot::DependencyFileNotParseable => e
    puts e.file_path
    puts e.cause.message
end

dependencies.select(&:top_level?).each do |dep|

	if isDepBlacklisted?(blacklist, dep.name)
		puts "  - " + dep.name + " is in the blacklist"
  	elsif issueAlreadyExists?(gitlab_hostname, gitlab_token, project_id, dep.name)
    	puts "  - " + dep.name + " issue already opened"
	else
		#########################################
		# Get update details for the dependency #
		#########################################
		checker = Dependabot::UpdateCheckers.for_package_manager(package_manager).new(
			dependency: dep,
			dependency_files: files,
			credentials: credentials,
			)

		next if checker.up_to_date?

		requirements_to_unlock =
			if !checker.requirements_unlocked_or_can_be?
				if checker.can_update?(requirements_to_unlock: :none) then :none
				else :update_not_possible
				end
			elsif checker.can_update?(requirements_to_unlock: :own) then :own
			elsif checker.can_update?(requirements_to_unlock: :all) then :all
			else :update_not_possible
			end

		next if requirements_to_unlock == :update_not_possible

		updated_deps = checker.updated_dependencies(
			requirements_to_unlock: requirements_to_unlock
		)

		#####################################
		# Generate updated dependency files #
		#####################################
		#
		print "  - Updating #{dep.name}…"
		updater = Dependabot::FileUpdaters.for_package_manager(package_manager).new(
			dependencies: updated_deps,
			dependency_files: files,
			credentials: credentials,
			)

		updated_files = updater.updated_dependency_files
		########################################
		# Create a pull request for the update #
		########################################
    	pr_creator = Dependabot::PullRequestCreator.new(
			source: source,
			base_commit: commit,
			dependencies: updated_deps,
			files: updated_files,
			credentials: credentials,
			label_language: true,
    	)
    	pull_request = pr_creator.create
    	puts " done"
  	end
end

puts "Done"
